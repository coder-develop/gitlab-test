package com.tao.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Package: com.tao.test
 * @Description:
 * @author: 赵涛
 * @date: 2020/9/22 16:05
 */
@RequestMapping
@RestController
public class UserController {

    @RequestMapping("get")
    public Map get(){
        Map map = new HashMap();
        map.put("1","the first..");
        return map;
    }
}
